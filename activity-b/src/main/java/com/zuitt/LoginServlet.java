package com.zuitt;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -7197591778254098037L;

	public void init() throws ServletException {
		System.out.println("Login has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		HttpSession session = req.getSession();
		String fname = session.getAttribute("fname").toString();
		String lname = session.getAttribute("lname").toString();
		String type = session.getAttribute("position").toString();
		
		session.setAttribute("fullname", fname + " " + lname);
		session.setAttribute("type", type);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("Login has been finalized.");
	}
}
