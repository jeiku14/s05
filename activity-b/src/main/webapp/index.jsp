<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Creating Dynamic Content</title>

	<style>
		div {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
</head>
<body>
	<h1>Welcome to Servlet Job Finder!</h1>
	
	<form method="post" action="register">
		<div>
			<label for="fname">First Name</label>
			<input type="text" name="fname" required>
		</div>
		
		<div>
			<label for="lname">Last Name</label>
			<input type="text" name="lname" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="text" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="text" name="email" required>
		</div>
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			<input type="radio" id="friends" name="source" required value="friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="socmed" name="source" required value="socmed">
			<label for="socmed">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="source" required value="others">
			<label for="others">Others</label>
			<br>		
		</fieldset>
		
		<div>
			<label for="birthdate">Date of birth</label>
			<input type="date" name="birthdate" required>
		</div>
		
		<div>
			<label for="position">Are you an employer or applicant?</label>
			<select id="hiring_position" name="position">
				<option value="applicant" selected="selected">Applicant</option>
				<option value="employer">Employer</option>
			</select>
		</div>
		
		<div>
			<label for="description">Profile Description</label>
			<textarea name="description" maxlength="500"></textarea>
		</div>
		
		<button>Register</button>
	</form>
</body>
</html>