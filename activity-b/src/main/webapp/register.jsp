<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="discovery.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration confirmation</title>
</head>
<body>

	<%
		String birthdate = session.getAttribute("birthdate").toString();
		birthdate = birthdate.replace("T", " - ");
	%>
	
	<h1>Registration Confirmation</h1>
	<p>First Name: <%=session.getAttribute("fname")%></p>
	<p>Last Name: <%=session.getAttribute("lname")%></p>
	<p>Phone: <%=session.getAttribute("phone")%></p>
	<p>Email: <%=session.getAttribute("email")%></p>
	<p>App Discovery: <%= source %></p>
	<p>Date of Birth: <%=birthdate %></p>
	<p>User Type: <%=session.getAttribute("position") %></p>
	<p>Description: <%=session.getAttribute("comments") %></p>
	
	<form action="login" method="post">
		<input type="submit">
	</form>
	
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>
</body>
</html>